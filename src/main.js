import Vue from 'vue'
import App from './App.vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import router from "./router";
import store from "./store";
import VCommentPicker from "./components/comment.vue";


Vue.use(Vuetify)
Vue.component("VCommentPicker", VCommentPicker);
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')