import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);
const store = new Vuex.Store({
  state: {
    token: "09865-08765-abcssawe",
    user: {
      id: "09865-08765-abc",
      name: 'Tô Thị Tuyết Nga',
      avatar: 'https://blogphotoshop.com/wp-content/uploads/2018/04/hinh-anh-dep-ve-tinh-yeu-buon.jpg',
    }
  },

});
export default store;