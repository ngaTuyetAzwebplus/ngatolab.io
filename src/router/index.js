import Vue from "vue";
import Router from "vue-router";
import Home from "../components/profile.vue";
import Demo from "../components/temp.vue";

Vue.use(Router);

const router = new Router({
  routes: [{
      path: "/",
      component: Home,
    },
    {
      path: "/demo",
      component: Demo,
    },

  ]
})
export default router;